#!/usr/bin/env bash

[[ $EUID -ne 0 ]] && echo -e "\nTry: sudo ./install.sh\n" && exit 1

echo -e "\nCopiando arquivos..."

cp slideshell /usr/bin/
cp slideshellrc /etc/

cp slideshell.1 /usr/share/man/man1/

mkdir -p /usr/share/doc/slideshell
cp README.md /usr/share/doc/slideshell/
cp LICENSE /usr/share/doc/slideshell/

gzip -9n /usr/share/man/man1/slideshell.1

echo -e "Pronto!\n"

